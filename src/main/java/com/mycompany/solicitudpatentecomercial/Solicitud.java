/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.solicitudpatentecomercial;

import com.mycompany.solicitudpatentecomercial.dao.SolicitudpatenteJpaController;
import com.mycompany.solicitudpatentecomercial.dao.exceptions.NonexistentEntityException;
import com.mycompany.solicitudpatentecomercial.entity.Solicitudpatente;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 *
 * @author Simon navarro
 */
@Path("/solicitud")
public class Solicitud {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response bucarSolicitudes(){

    Solicitudpatente sol1=new Solicitudpatente();
    sol1.setNombre("5");
    sol1.setRut("19");
    sol1.setDireccion("5");
    sol1.setActividad("5");

    Solicitudpatente sol2=new Solicitudpatente();
    sol2.setNombre("8");
    sol2.setRut("18");
    sol2.setDireccion("6");
    sol2.setActividad("9");

    List<Solicitudpatente> lista= new ArrayList<Solicitudpatente>();
 
    lista.add(sol1);
    lista.add(sol2);
    SolicitudpatenteJpaController dao=new SolicitudpatenteJpaController();
    
    List<Solicitudpatente> list= dao.findSolicitudpatenteEntities();
    
    return Response.ok(200).entity(lista).build();

    }
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Solicitudpatente solicitudpatente){
              
       
        try {
            SolicitudpatenteJpaController dao=new SolicitudpatenteJpaController();
            dao.create(solicitudpatente);
        } catch (Exception ex) {
            Logger.getLogger(Solicitud.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(solicitudpatente).build();
        
    }
    @DELETE
    @Path("*/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete")String iddelete){
        try {
            SolicitudpatenteJpaController dao=new SolicitudpatenteJpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Solicitud.class.getName()).log(Level.SEVERE, null, ex);
        }
        
     return Response.ok("solicitud eliminada").build();
    
 }
    @PUT
    public Response Update(Solicitudpatente solicitudpatente){
        try {
            SolicitudpatenteJpaController dao=new SolicitudpatenteJpaController();
            dao.edit(solicitudpatente);
        } catch (Exception ex) {
            Logger.getLogger(Solicitud.class.getName()).log(Level.SEVERE, null, ex);
        }  
             
        
        return Response.ok(200).entity(solicitudpatente).build();
    }
  
}  

    