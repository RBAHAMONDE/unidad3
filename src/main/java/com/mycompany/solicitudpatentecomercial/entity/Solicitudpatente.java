/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.solicitudpatentecomercial.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Simon navarro
 */
@Entity
@Table(name = "solicitudpatente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Solicitudpatente.findAll", query = "SELECT s FROM Solicitudpatente s"),
    @NamedQuery(name = "Solicitudpatente.findByNombre", query = "SELECT s FROM Solicitudpatente s WHERE s.nombre = :nombre"),
    @NamedQuery(name = "Solicitudpatente.findByRut", query = "SELECT s FROM Solicitudpatente s WHERE s.rut = :rut"),
    @NamedQuery(name = "Solicitudpatente.findByDireccion", query = "SELECT s FROM Solicitudpatente s WHERE s.direccion = :direccion"),
    @NamedQuery(name = "Solicitudpatente.findByActividad", query = "SELECT s FROM Solicitudpatente s WHERE s.actividad = :actividad")})
public class Solicitudpatente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 2147483647)
    @Column(name = "actividad")
    private String actividad;

    public Solicitudpatente() {
    }

    public Solicitudpatente(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombre != null ? nombre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Solicitudpatente)) {
            return false;
        }
        Solicitudpatente other = (Solicitudpatente) object;
        if ((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.solicitudpatentecomercial.entity.Solicitudpatente[ nombre=" + nombre + " ]";
    }
    
}
