/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.solicitudpatentecomercial.dao;

import com.mycompany.solicitudpatentecomercial.dao.exceptions.NonexistentEntityException;
import com.mycompany.solicitudpatentecomercial.dao.exceptions.PreexistingEntityException;
import com.mycompany.solicitudpatentecomercial.entity.Solicitudpatente;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Simon navarro
 */
public class SolicitudpatenteJpaController implements Serializable {

    public SolicitudpatenteJpaController() {
        }

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Solicitudpatente solicitudpatente) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(solicitudpatente);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSolicitudpatente(solicitudpatente.getNombre()) != null) {
                throw new PreexistingEntityException("Solicitudpatente " + solicitudpatente + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Solicitudpatente solicitudpatente) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            solicitudpatente = em.merge(solicitudpatente);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = solicitudpatente.getNombre();
                if (findSolicitudpatente(id) == null) {
                    throw new NonexistentEntityException("The solicitudpatente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Solicitudpatente solicitudpatente;
            try {
                solicitudpatente = em.getReference(Solicitudpatente.class, id);
                solicitudpatente.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The solicitudpatente with id " + id + " no longer exists.", enfe);
            }
            em.remove(solicitudpatente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Solicitudpatente> findSolicitudpatenteEntities() {
        return findSolicitudpatenteEntities(true, -1, -1);
    }

    public List<Solicitudpatente> findSolicitudpatenteEntities(int maxResults, int firstResult) {
        return findSolicitudpatenteEntities(false, maxResults, firstResult);
    }

    private List<Solicitudpatente> findSolicitudpatenteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Solicitudpatente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Solicitudpatente findSolicitudpatente(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Solicitudpatente.class, id);
        } finally {
            em.close();
        }
    }

    public int getSolicitudpatenteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Solicitudpatente> rt = cq.from(Solicitudpatente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
